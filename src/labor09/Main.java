package labor09;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        try {
            List<Schueler> test = SchuelerUtils.getTestKlasse();
            List<Schueler> csv = Files.lines(Paths.get("FILES/schueler12.csv")).
                    map(SchuelerUtils::fromCSV).collect(Collectors.toList());
            System.out.println("--1--");
            System.out.println(test.stream()
                    .filter(Schueler::isWeiblich)
                    .count()
            );
            System.out.println("--2--");
            System.out.println(test.stream()
                    .filter(Schueler::isWeiblich)
                    .map(Schueler::getVorname)
                    .reduce((a,b)->a+", "+b).get()
            );
            System.out.println("--3--");
            csv.stream()
                    .filter(Schueler::isWeiblich)
                    .sorted((a,b)->{ 
                        if(a.getKlasse().compareTo(b.getKlasse())==0){
                            return a.getNr()-b.getNr();
                        }else{
                            return a.getKlasse().compareTo(b.getKlasse());
                        }                
            }).forEach(System.out::println);
            System.out.println("--4--");
            csv.stream()
                    .filter(i-> !i.isWeiblich())
                    .filter(i-> i.getKlasse().charAt(0)=='4')
                    .filter(i -> i.getVorname().equals("Lukas"))
                    .forEach(System.out::println);
            System.out.println("--5--");
            System.out.println(csv.stream()
                    .mapToInt(i-> i.getVorname().length()+i.getNachname().length())
                    .max().getAsInt()
            );
            System.out.println("--6--");
            System.out.println(csv.stream()
                    .filter(Schueler::isWeiblich)
                    .filter(i->i.getVorname().contains("Julia"))
                    .map(Schueler::toString)
                    .findFirst().orElse("keine Julia")                    
            );
            System.out.println("--7--");
            System.out.println(csv.stream()
                    .map(Schueler::getKlasse)
                    .sorted()
                    .distinct()
                    .reduce((a,b)->a+", "+b).get()            
            );
            System.out.println("--8--");
            Map<String, Long> map = csv.stream().
                    collect(Collectors.groupingBy(Schueler::getKlasse,Collectors.counting()));
            System.out.println(new TreeMap<>(map));
            
            
            
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
