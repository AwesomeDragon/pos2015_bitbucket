/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labor00_2;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author awesome
 */
public class PrimeTest extends Thread{
    public static final long SLEEP = 1000;
    private static int threads= 0;    
    private int n;

    public PrimeTest(int n) {
        this.n = n;
    }

    public static int getThreads() {
        return threads;
    }
    
    @Override
    public void run() {
        PrimeTest.threads += 1;
        int till = (int) Math.ceil(Math.sqrt(n));
        try {
            for (int i = 2; i <= till; i++) {
                if (n % i == 0) {
                    PrimeTest.threads -= 1;
                    return;
                }
                Thread.sleep(SLEEP);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(PrimeTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("PRIME FOUND AT: " + n);
        PrimeTest.threads -= 1;
    }

}
