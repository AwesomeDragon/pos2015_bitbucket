/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labor00_2;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author awesome
 */
public class PrimeCalculator {
    private static final int SLEEP = 500;
    private static final int INTERVAL_START = 6;
    private static final int INTERVAL_END = 20;

    public static void main(String[] args) {
        Thread displayRunning = new Thread() {
            @Override
            public void run() {
                while (true) {
                    System.out.println("PrimeTest Threads Running:" + PrimeTest.getThreads());
                    try {
                        Thread.sleep(PrimeCalculator.SLEEP);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(PrimeCalculator.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        displayRunning.setDaemon(true);
        displayRunning.start();
        for (int i = INTERVAL_START; i <= INTERVAL_END; i++) {
            new PrimeTest(i).start();
        }
    }
}
