/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labor01_1;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author awesome
 */
public class Worker extends Thread {
    public static long SLEEP = 1000;
    private static int FROM[] = {5, 12, 2};
    private static int TO[] = {15, 18, 14};
    private int stadium = 0;
    private static Random rand = new Random();

    public int getStadium() {
        return stadium;
    }

    @Override
    public void run(){
        while (stadium < 3) {
            try {
                Thread.sleep(Worker.SLEEP * (rand.nextInt() % (TO[stadium] - FROM[stadium]+1) + TO[stadium]));
            } catch (InterruptedException ex) {
                Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
            }
            stadium++;
        }
    }

}
