/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labor01_1;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author awesome
 */
public class MainWorker {
    
    public static void main(String[] args) {
        final Worker[] workers = new Worker[100];
        for (int i = 0; i < 100; i++) {
            workers[i] = new Worker();
            workers[i].start();
        }
        Thread statistic = new Thread() {
            @Override
            public void run() {
                try {
                    int tmp=0;
                    while (true) {
                        int[] s = new int[4];
                        for (Worker w : workers) {
                            s[w.getStadium()]++;
                        }
                        System.out.println("#"+ tmp++ + "\t1: " + s[0] + "\t2: " + s[1] + "\t3: " + s[2] + "\tFIN: " + s[3]);
                        if(s[3]==100){
                            System.out.println("ALLE FERTIG");return;
                        }
                        Thread.sleep(Worker.SLEEP);
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(MainWorker.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        statistic.start();
    }
}
