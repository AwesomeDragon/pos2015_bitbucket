package labor00_1;

public class Worker implements Runnable {

    private int intervall_start;
    private int intervall_end;
    private double solution;
    private boolean ready = false;

    public Worker(int intervall_start, int intervall_end) {
        this.intervall_start = intervall_start;
        this.intervall_end = intervall_end;
    }

    public double getSolution() {
        if (!ready) {
            throw new IllegalStateException("Result not available yet");
        }
        return solution;
    }

    @Override
    public void run() {
        for(int n = intervall_start; n  <= intervall_end; n++) {
            int vz = (n % 2 == 0) ? 1 : -1;
            solution += vz / (2.0 * n + 1.0);
        }
        ready = true;
    }

}
