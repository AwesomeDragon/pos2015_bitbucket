package labor00_1;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static int N = 100000;
    private static int T = 10;//Number of Threads
    private static double SOL = 0;

    public static void main(String[] args) {   
        Thread[] threads = new Thread[T];
        Worker[] workers = new Worker[T];
        
        int size = N / T;
        int remainder=N % T; 
        int start=0;
        for (int i = 0; i < T; i++) {
            int end=start+size-1;
            if(remainder>0){
                end++;
                remainder--;
            }
            System.out.println("#"+i+": "+start+" - "+end);
            workers[i] = new Worker(start,end);
            start=end+1;
       }        
        for (int i = 0; i < T; i++) {
            threads[i] = new Thread(workers[i]);
            threads[i].start();
        }
        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        for (Worker w : workers) {
            SOL += w.getSolution();
        }
        System.out.println("SOLUTION: " + SOL*4);
    }
}

