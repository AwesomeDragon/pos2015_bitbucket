package labor08;

import java.math.BigInteger;
import java.util.*;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.stream.*;

public class Streams {

    private static final int bsp = 9;
    private static Random random = new Random();

    private static void f1() {
        IntStream.range(1, 21).filter(i -> i % 2 != 0).map(i -> i * i).forEach(System.out::println);
    }

    private static void f2() {
        System.out.println(DoubleStream.iterate(1, i -> i + 1).limit(100).map(i -> 1 / ((i + 1) * (i + 2))).sum());
    }

    private static void f3() {
        IntStream.generate(() -> random.nextInt(45) + 1).distinct().limit(6).sorted().forEach(System.out::println);
    }

    private static void f4() {
        int size = 20, max = 20;
        int a[] = IntStream.generate(() -> random.nextInt(max)).limit(size).toArray();
        IntStream.of(a).filter(i -> i % 2 != 0).map(i -> i * i).sorted().forEach(System.out::println);
    }

    private static void f5() {
        LongStream.range(1, 21).reduce((i, j) -> i * j).ifPresent(System.out::println);
    }

    private static void f6() {
        System.out.println(
                IntStream.range(1, 1001).mapToObj(Integer::toString).reduce(String::concat).get().chars().filter(i -> i == '1').count()
        );
    }

    private static void f7() {
        LongStream.iterate(1, i -> i * (i + 1)).limit(6).forEach(System.out::println);
        //1,2,6,42,1806,3_263_442 
    }

    static class FaktSupplier implements Supplier<BigInteger> {
        public static Map<BigInteger,Integer> map=new HashMap<>();
        private int i = 0;
        private BigInteger faktorielle = BigInteger.ONE;

        @Override
        public BigInteger get() {
            faktorielle = faktorielle.multiply(BigInteger.valueOf(++i));
            map.put(faktorielle, i);
            return faktorielle;
        }
    };

    private static void f8() {
        Stream.generate(new FaktSupplier()).filter(i -> i.toString().length() > 999).findFirst().ifPresent(System.out::println);
    }

    private static void f9() {
        BigInteger wanted = BigInteger.TEN.pow(10000);
        Optional<BigInteger> erg = Stream.generate(new FaktSupplier()).filter(i -> i.compareTo(wanted)>0).findFirst();
        System.out.println(FaktSupplier.map.get(erg.get()));
    }

    static class FibbSupplier implements Supplier<BigInteger> {
        public static Map<BigInteger,Integer> map=new HashMap<>();
        private BigInteger a=BigInteger.ONE,b=BigInteger.ONE,tmp;
        private int n=0;
        @Override
        public BigInteger get() {
            if (++n<3) {
                return BigInteger.ONE;
            }
            tmp = a.add(b);
            a = b;
            b = tmp;
            map.put(b,n);
            return b;
        }
    };

    private static void f10() {
        BigInteger wanted = BigInteger.TEN.pow(999);//1000 digits
        Optional<BigInteger> erg = Stream.generate(new FibbSupplier()).filter(i -> i.compareTo(wanted)>0).findFirst();
        System.out.println(FibbSupplier.map.get(erg.get()));
    }

    private static void f11() {
        int a[] = IntStream.range(1, 6).boxed().map(i -> IntStream.range(0, i + 1)).flatMapToInt(i -> i).toArray();
        IntStream.of(a).forEach(System.out::println);
    }

    private static void f12() {
        System.out.println(IntStream.range(1, 101).boxed().map(i -> IntStream.generate(() -> i).limit(i)).flatMapToInt(i -> i).sum());
    }

    public static void main(String[] args) {
        switch (bsp) {
            case 1:
                System.out.println("----1----");
                f1();
                break;
            case 2:
                System.out.println("----2----");
                f2();
                break;
            case 3:
                System.out.println("----3----");
                f3();
                break;
            case 4:
                System.out.println("----4----");
                f4();
                break;
            case 5:
                System.out.println("----5----");
                f5();
                break;
            case 6:
                System.out.println("----6----");
                f6();
                break;
            case 7:
                System.out.println("----7----");
                f7();
                break;
            case 8:
                System.out.println("----8----");
                f8();
                break;
            case 9:
                System.out.println("----9----");
                f9();
                break;
            case 10:
                System.out.println("----10----");
                f10();
                break;
            case 11:
                System.out.println("----11----");
                f11();
                break;
            case 12:
                System.out.println("----12----");
                f12();
                break;

        }
    }
}
