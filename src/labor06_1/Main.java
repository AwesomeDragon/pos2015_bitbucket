package labor06_1;

import com.sun.javafx.geom.ConcentricShapePair;

public class Main {
    public static Subject_ColorChooser subject=null;
    public static void main(String[] args) throws InterruptedException {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                subject = new Subject_ColorChooser();
                subject.setVisible(true);
            }
        });
        while(subject==null)Thread.sleep(200);
        ConcreteObserver_DisplayColor.main(args);
        ConcreteObserver_Log.main(args);
    }
}
