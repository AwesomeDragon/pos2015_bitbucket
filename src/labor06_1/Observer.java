package labor06_1;

import java.awt.Color;

public interface Observer {
    public void doWithColor(Color color);
}
