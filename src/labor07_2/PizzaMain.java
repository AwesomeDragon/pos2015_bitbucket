/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labor07_2;

/**
 *
 * @author awesome
 */
public class PizzaMain {

    public static void main(String[] args) {
        AbstractPizza pizza;
        System.out.println("----------SalamiPizza--------------------");
        pizza=new SalamiPizza(new Pizza());
        pizza.zeigeBelag();
        System.out.println("SUM: "+pizza.betrag());
        System.out.println("----------SalamiTonnoPizza---------------");
        pizza=new TonnoPizza(new SalamiPizza(new Pizza()));
        pizza.zeigeBelag();
        System.out.println("SUM: "+pizza.betrag());
        System.out.println("----------DeluxePizza--------------------");
        pizza=new CalamariPizza(new TonnoPizza(new SalamiPizza(new Pizza())));
        pizza.zeigeBelag();
        System.out.println("SUM: "+pizza.betrag());
    }

    public static abstract class AbstractPizza {
        /**
         * Zeigt alle Belaege der Pizza
         */
        public abstract void zeigeBelag();

        /**
         * Liefert den Gesamtpreis der Pizza. Eine leere Pizza hat einen Grundpreis und jeder Belag erhoeht den Gesamtpreis der Pizza.
         */
        public abstract double betrag();
    }
    
    public static class Pizza extends AbstractPizza{
        private static final double price = 6.50;
        
        @Override
        public void zeigeBelag(){
            System.out.println("und natürlich Pizzateig");
        }

        @Override
        public double betrag() {
            return price;
        }
    }
    
    public abstract static class PizzaWithTopping extends AbstractPizza{
        public AbstractPizza pizza;
        
        public abstract void zeigeBelag();
        public abstract double betrag();
        
        public PizzaWithTopping(AbstractPizza pizza) {
            this.pizza=pizza;
        }
    }
    
    public static class SalamiPizza extends PizzaWithTopping{
        public final double price = 1;
        public final String topping = "Salami";
        
        public SalamiPizza(AbstractPizza pizza) {
            super(pizza);
        }
        @Override
        public void zeigeBelag(){
            System.out.println(topping);
            pizza.zeigeBelag();
        }

        @Override
        public double betrag() {
            return price+pizza.betrag();
        }
    }
    public static class TonnoPizza extends PizzaWithTopping{
        public final double price = 2.50;
        public final String topping = "Tonno";
        
        public TonnoPizza(AbstractPizza pizza) {
            super(pizza);
        }
        @Override
        public void zeigeBelag(){
            System.out.println(topping);
            pizza.zeigeBelag();
        }

        @Override
        public double betrag() {
            return price+pizza.betrag();
        }
    }
    public static class CalamariPizza extends PizzaWithTopping{
        public final double price = 4;
        public final String topping = "Calamari";
        
        public CalamariPizza(AbstractPizza pizza) {
            super(pizza);
        }
        @Override
        public void zeigeBelag(){
            System.out.println(topping);
            pizza.zeigeBelag();
        }

        @Override
        public double betrag() {
            return price+pizza.betrag();
        }
    }
}
