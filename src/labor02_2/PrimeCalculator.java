package labor02_2;

import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import labor00_2.PrimeTest;

public class PrimeCalculator {
    private static final int SLEEP = 500;
    private static final int INTERVAL_START = 6;
    private static final int INTERVAL_END = 20;
    private static final ExecutorService executor = Executors.newFixedThreadPool(Math.min((INTERVAL_END-INTERVAL_START)/2,100));

    public static void main(String[] args) {
        Thread displayRunning = new Thread() {
            @Override
            public void run() {
                while (true) {
                    System.out.println("PrimeTest Threads Running:" + PrimeTest.getThreads());
                    try {
                        Thread.sleep(PrimeCalculator.SLEEP);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(PrimeCalculator.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        displayRunning.setDaemon(true);
        displayRunning.start();
        for (int i = INTERVAL_START; i <= INTERVAL_END; i++) {
            executor.execute(new PrimeTest(i));
        }
        executor.shutdown();
    }
}

