package labor06_2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        try {
            Files.lines(Paths.get("FILES/Labor_06_schueler.csv")).forEach(Pupil::add);
            System.out.println(Pupil.list);
            MainFrame.main(args);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
