/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labor06_2;

import java.util.*;

/**
 *
 * @author awesome
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
        update();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TextArea = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        FilterFemale = new javax.swing.JCheckBox();
        FilterClass1 = new javax.swing.JCheckBox();
        FilterClassA = new javax.swing.JCheckBox();
        SortClassNumber = new javax.swing.JCheckBox();
        SortAlphabet = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("List"));

        TextArea.setColumns(20);
        TextArea.setRows(5);
        jScrollPane1.setViewportView(TextArea);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 516, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Filter and Sorter"));

        FilterFemale.setText("Filter Female");
        FilterFemale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FilterFemaleActionPerformed(evt);
            }
        });

        FilterClass1.setText("Filter Class 1");
        FilterClass1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FilterClass1ActionPerformed(evt);
            }
        });

        FilterClassA.setText("Filter Class A");
        FilterClassA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FilterClassAActionPerformed(evt);
            }
        });

        SortClassNumber.setText("Sort Class & Number");
        SortClassNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SortClassNumberActionPerformed(evt);
            }
        });

        SortAlphabet.setText("Sort Alphabet");
        SortAlphabet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SortAlphabetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(FilterFemale)
                    .addComponent(FilterClass1)
                    .addComponent(FilterClassA)
                    .addComponent(SortClassNumber)
                    .addComponent(SortAlphabet))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(FilterFemale)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(FilterClass1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(FilterClassA)
                .addGap(18, 18, 18)
                .addComponent(SortClassNumber)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(SortAlphabet)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void FilterFemaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FilterFemaleActionPerformed
        update();
    }//GEN-LAST:event_FilterFemaleActionPerformed

    private void FilterClass1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FilterClass1ActionPerformed
        update();
    }//GEN-LAST:event_FilterClass1ActionPerformed

    private void FilterClassAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FilterClassAActionPerformed
        update();
    }//GEN-LAST:event_FilterClassAActionPerformed

    private void SortClassNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SortClassNumberActionPerformed
        update();
        SortAlphabet.setSelected(false);
    }//GEN-LAST:event_SortClassNumberActionPerformed

    private void SortAlphabetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SortAlphabetActionPerformed
        update();
        SortClassNumber.setSelected(false);
    }//GEN-LAST:event_SortAlphabetActionPerformed

    private void update(){
        List<SortStrategy> sorters = new ArrayList<>();
        List<FilterStrategy> filters = new ArrayList<>();
        if(FilterClass1.isSelected())filters.add(new StrategyImpl.FilterStrategyFirstClass());
        if(FilterClassA.isSelected())filters.add(new StrategyImpl.FilterStrategyAClass());
        if(FilterFemale.isSelected())filters.add(new StrategyImpl.FilterStrategyFemale());
        if(SortAlphabet.isSelected())sorters.add(new StrategyImpl.SortStrategyAlphabet());
        if(SortClassNumber.isSelected())sorters.add(new StrategyImpl.SortStrategyUnitNumber());
        
        List<Pupil> l = Pupil.getList();
        for(SortStrategy s : sorters){
            l=s.sortPupilList(l);
        }
        for(FilterStrategy f : filters){
            l=f.filterPupilList(l);
        }
        TextArea.setText("");
        for(Pupil p : l){
            TextArea.append(p.toString()+"\n");
        }
        
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox FilterClass1;
    private javax.swing.JCheckBox FilterClassA;
    private javax.swing.JCheckBox FilterFemale;
    private javax.swing.JCheckBox SortAlphabet;
    private javax.swing.JCheckBox SortClassNumber;
    private javax.swing.JTextArea TextArea;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
