package labor06_2;

public enum Gender {
    MALE,
    FEMALE;
    public char getChar(){
        return (this.equals(Gender.MALE))?'M':'W';
    }
}
