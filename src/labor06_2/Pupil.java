package labor06_2;

import java.util.*;

public class Pupil {
    public static List<Pupil> list = new ArrayList<>();

    private String lastname;
    private String firstname;
    private Gender gender;
    private int number;
    private String unit;
    private char state;

    public static boolean add(String csv){
        try{
            list.add(new Pupil(csv));
            return true;
        }catch(IllegalArgumentException ex){
            return false;
        }
    }
    
    private Pupil(String csv) throws IllegalArgumentException {
        try {
            String vals[] = csv.split(";");
            lastname = vals[0];
            firstname = vals[1];
            gender = ("M".equals(vals[2])) ? Gender.MALE : Gender.FEMALE;
            number = Integer.parseInt(vals[3]);
            unit = vals[4];
            state = vals[5].charAt(0);
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    @Override
    public String toString() {
        return gender.getChar()+"\t"+state+"\t"+unit+"\t"+number+"\t"+lastname+" "+firstname;
    }

    public static List<Pupil> getList() {
        return list;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public Gender getGender() {
        return gender;
    }

    public int getNumber() {
        return number;
    }

    public String getUnit() {
        return unit;
    }

    public char getState() {
        return state;
    }
    
    
}
