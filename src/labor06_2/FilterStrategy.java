package labor06_2;

import java.util.*;

public interface FilterStrategy {
    List<Pupil> filterPupilList(List<Pupil> pupilList);
}
