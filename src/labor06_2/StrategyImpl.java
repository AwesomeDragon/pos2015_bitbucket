package labor06_2;

import java.util.*;
import java.util.stream.Collectors;

public abstract class StrategyImpl {

    public static class FilterStrategyFemale implements FilterStrategy {

        @Override
        public List<Pupil> filterPupilList(List<Pupil> pupilList) {
            return pupilList.stream().
                    filter(t->t.getGender().equals(Gender.FEMALE)).
                    collect(Collectors.toList());
        }

    }
    public static class FilterStrategyFirstClass implements FilterStrategy {

        @Override
        public List<Pupil> filterPupilList(List<Pupil> pupilList) {
            return pupilList.stream().
                    filter(t->t.getUnit().charAt(0)=='1').
                    collect(Collectors.toList());
        }

    }

    public static class FilterStrategyAClass implements FilterStrategy {

        @Override
        public List<Pupil> filterPupilList(List<Pupil> pupilList) {
            return pupilList.stream().
                    filter(t->t.getUnit().charAt(1)=='A').
                    collect(Collectors.toList());
        }

    }

    public static class SortStrategyUnitNumber implements SortStrategy {

        @Override
        public List<Pupil> sortPupilList(List<Pupil> pupilList) {
            List<Pupil> sorted = new ArrayList<>(pupilList);
            sorted.sort((Pupil a,Pupil b)->{
                int tmp = a.getUnit().compareTo(b.getUnit());
                if(tmp==0){
                    return a.getNumber()-b.getNumber();
                }
                return tmp;
            });
            return sorted;
        } 

    }

    public static class SortStrategyAlphabet implements SortStrategy {

        @Override
        public List<Pupil> sortPupilList(List<Pupil> pupilList) {
            List<Pupil> sorted = new ArrayList<>(pupilList);
            sorted.sort((Pupil a, Pupil b) -> {
                int tmp = a.getLastname().compareTo(b.getLastname());
                if (tmp == 0) {
                    return a.getFirstname().compareTo(b.getFirstname());
                }
                return tmp;
            });
            return sorted;
        }
    }
}
