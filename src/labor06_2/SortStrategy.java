package labor06_2;

import java.util.*;

public interface SortStrategy {
    List<Pupil> sortPupilList(List<Pupil> pupilList);
}
