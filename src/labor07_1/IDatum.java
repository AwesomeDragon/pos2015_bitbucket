package labor07_1;

public interface IDatum extends Comparable<IDatum>{
/**
* Liefert das Jahr dieses Datums
* @return das Jahr
*/
int getJahr();
/**
* Liefert das Monat dieses Datums
* @return das Monat, einen Wert im Bereich 1-12
*/
int getMonat();
/**
* Liefert den Namen des Monats
* @return den Monatsnamen (Jaenner, Februar, Maerz,
* April, Mai, Juni, Juli, August, September,
* Oktober, November, Dezember
*/
String getMonatByString();
/**
* Liefert den Tag dieses Datums
* @return den tag im Monat (1 - 31)
*/
int getTag();
/**
* Erhoeht (vermindert) das gespeicherte Datum um t Tage.
* Liegt nach dieser Operation das Datum vor dem 1.1.1900,
* so wirft die Methode die checked IllegalDateException
* @param t Anzahl der Tage, um die dieses Datum erhoeht
* (t > 0) bzw. vermindert (t < 0) wird
*/
void addiereTage(int t);
/**
* Liefert die Nummer des Wochentages.
* @return Nummer des Wochentages im Bereich von
* 0 (Montag) bis 6 (Sonntag)
*/
int wochentagNummer();
/**
* Liefert den Wochentag als String
* @return Wochentag als String
* Montag, Dienstag, Mittwoch, Donnerstag,
* Freitag Samstag, Sonntag.
*/
String wochentag();
/**
* Liefert die zwischen dem this-Objekt und dem
* uebergebenen Datum other vergangenen Tage.
* @param other das andere Datum
* @return Tage zwischen this und other, postiv wenn other nach this
* liegt, sonst negativ bzw. 0
*/
int tageZwischen(IDatum other);
/**
* Vergleicht dieses Datum mit other
* @param other Datum, mit dem dieses Datum verglichen wird
* @return negativ, wenn other spaeter liegt, positiv, wenn d frueher
* liegt und 0 bei gleichem Datum
*/
@Override
int compareTo(IDatum d);
/**
* Prueft, ob das aktuelle Datum in einem Schaltjahr liegt
* @return true, wenn Schaltjahr false sonst.
*/
boolean isSchaltjahr();
/**
* Liefert eine Stringdarstellung in der Form
* <code>tt.mm.jjjj</code>
* @return Stringdarstellung in der Form <code>tt.mm.jjjj</code>
*/
@Override
String toString();
}
