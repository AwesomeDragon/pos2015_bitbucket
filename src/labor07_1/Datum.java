/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labor07_1;

import java.time.*;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author awesome
 */
public class Datum implements IDatum {

    java.time.LocalDate date;

    /**
     * Erzeugt eine Datumsinstanz mit der aktuellen Systemzeit
     */
    public Datum() {
        date = LocalDate.now();
    }

    /**
     * Erzeugt eine Datumsinstanz, die t Tage nach dem 1.1.1900 liegt. Bei
     * negativem t wird die checked IllegalDateException geworfen.
     *
     * @param t Tage seit dem 1.1.1900
     */
    public Datum(int t) {
        date = LocalDate.of(1900, 1, 1).plusDays(t);
    }

    /**
     * Erzeugt eine Datumsinstanz mit den gegebenen Werten. Bei einem
     * ungueltigen Datum wird die checked IllegalDateException geworfen.
     *
     * @param tag der Tag 1-31 ( abhaengig vom Monat)
     * @param monat das Monat, 1 - 12
     * @param jahr das Jahr, groesser oder gleich 1900
     */
    public Datum(int tag, int monat, int jahr) {
        date = LocalDate.of(jahr, monat, tag);
    }
    
    @Override
    public int getJahr() {
        return date.getYear();
    }

    @Override
    public int getMonat() {
        return date.getMonthValue();
    }

    @Override
    public String getMonatByString() {
        return date.getMonth().toString();
    }

    @Override
    public int getTag() {
        return date.getDayOfMonth();
    }

    @Override
    public void addiereTage(int t) {
        date = date.plusDays(t);
    }

    @Override
    public int wochentagNummer() {
        return date.getDayOfWeek().getValue() - 1;
    }

    @Override
    public String wochentag() {
        return date.getDayOfWeek().toString();
    }

    @Override
    public int tageZwischen(IDatum other) {
        return (int)ChronoUnit.DAYS.between(date,LocalDate.of(other.getJahr(), other.getMonat(), other.getTag()));
    }

    @Override
    public int compareTo(IDatum d) {
        return date.compareTo(LocalDate.of(d.getJahr(), d.getMonat(), d.getTag()));
    }

    @Override
    public boolean isSchaltjahr() {
        return date.isLeapYear();
    }
}
