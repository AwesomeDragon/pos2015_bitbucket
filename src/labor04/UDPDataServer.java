/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package udpdata;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author reio
 */
public class UDPDataServer {

  /**
   * @param args the command line arguments
   */
  private static Random rd = new Random();
  private static String client = "localhost";
  public static int port = 20012;

  public static void main(String[] args) {

    try (DatagramSocket s = new DatagramSocket()) {
      while (true) {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          // Nothing to do
        }
        try {
          ByteArrayOutputStream bos = new ByteArrayOutputStream(40);
          DataOutputStream dos = new DataOutputStream((bos));
          for (int i = 0; i < 10; i++) {
            dos.writeInt(rd.nextInt(20) + 1);
          }
          bos.close();
          byte[] data = bos.toByteArray();
          DatagramPacket pack = new DatagramPacket(data, 
                                                   data.length,
                                                   InetAddress.getByName(client),
                                                   port);
          s.send(pack);
          System.out.println("Packet sent");
        } catch (IOException ex) {
          Logger.getLogger(UDPDataServer.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    } catch (SocketException ex) {
      Logger.getLogger(UDPDataServer.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}
