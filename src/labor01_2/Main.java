/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labor01_2;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author awesome
 */
public class Main {

    private static final int STACKSIZE = 5;
    private static final int READ_SPEED = 700;//the lower the faster
    private static final int WRITE_SPEED = 700;//the lower the faster
    private static final Stack<Integer> s = new Stack<>(STACKSIZE);
    private static final Random rand = new Random();
    private static int WRITE_COUNT;

    public static void main(String[] args) {
        Thread writer = new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        synchronized (s) {
                            while (s.isFull()) {
                                System.out.println("WRITER: waiting");
                                s.wait();
                                System.out.println("WRITER: stop waiting");
                            }
                            s.push(++WRITE_COUNT);
                            System.out.println("WRITER: wrote " + WRITE_COUNT + "\tnotify\t"+s);
                            s.notify();
                        }
                        Thread.sleep((int) (rand.nextInt(WRITE_SPEED) + 100));
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        Thread reader = new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        synchronized (s) {
                            while (s.isEmpty()) {
                                System.out.println("READER: waiting");
                                s.wait();
                                System.out.println("READER: stop waiting");
                            }
                            System.out.println("READER: read " + s.pop() + "\tnotify\t"+s);
                            s.notify();
                        }
                        Thread.sleep((int) (rand.nextInt(READ_SPEED) + 100));
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        writer.start();
        reader.start();
    }
}
