/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labor01_2;

import java.util.Arrays;

/**
 *
 * @author awesome
 */
public class Stack<E> {

    private int n;
    private E[] s; // Datenfeld
    private int tos=-1; // Index des obersten Elementes
    
    @SuppressWarnings("unchecked")
    public Stack(int n) {
        this.n = n;
        s = (E[]) new Object[n];
    }

    /**
     * legt ein neues Objekt auf den Stack
     * 
     * @param o
     */
    public synchronized void push(E o){
        if(isFull()){
            throw new IllegalStateException("Tried to push to full stack.");
        }
        tos++;
        s[tos]=o;
    }

    /**
     * entfernt und liefert das oberste Objekt
     *
     * @return
     */
    public synchronized E pop() {
        if(isEmpty()){
            throw new IllegalStateException("Tried to read from full stack.");
        }
        tos--;
        return s[tos + 1];
    }

    /**
     * liefert true, wenn der Stack voll ist
     * 
     * @return
     */
    public boolean isFull() {
        return tos==n-1;
    }
    
    /**
     * liefert true, wenn der Stack empty ist.
     * @return 
     */
    public boolean isEmpty(){
        return tos==-1;
    }

    @Override
    public String toString() {
        StringBuilder tmp = new StringBuilder("Stack: ");
        for(int i=0;i<=tos;i++){
            tmp.append(s[i]+" ");
        }
        return tmp.toString();
    }
    
}
