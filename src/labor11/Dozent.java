package labor11;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class Dozent {
    Integer id;
    String zuname;
    String vorname;
    
    void setId(int id){
        if(this.id!=null)throw new IllegalStateException();
        this.id=id;
    }
}
