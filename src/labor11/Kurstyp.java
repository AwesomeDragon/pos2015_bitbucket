package labor11;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class Kurstyp {
    Character id;
    String name;
    
    void setId(char id){
        if(this.id!=null)throw new IllegalStateException();
        this.id=id;
    }
}
