/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labor11;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author awesome
 */
public class PostgresDB implements IDAO_Kurse {

    Connection con;
    PreparedStatement bucheKurs;
    PreparedStatement deleteKundePrep;
    PreparedStatement deleteKunde;
    PreparedStatement deleteKurstyp;
    PreparedStatement getDozenten;
    PreparedStatement getKunden;
    PreparedStatement getKundenFromKurs;
    PreparedStatement getKurse;
    PreparedStatement getKurstypen;
    PreparedStatement insertKunde;
    PreparedStatement insertKurs;
    PreparedStatement insertKurstyp;
    PreparedStatement storniereKurs;

    @Override
    public void connect() throws KursDBException {
        try {
            Driver driver = (Driver) Class.forName("org.postgresql.Driver").newInstance();
            Properties p = new Properties();
            p.put("user", "4AHIF_15");
            p.put("password", "jeke26");
            con = driver.connect("jdbc:postgresql://193.170.118.29:54322/4A_12_lab11", p);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            throw new KursDBException(ex);
        }
    }

    @Override
    public void disconnect() throws KursDBException {
        try {
            con.close();
        } catch (SQLException ex) {
            throw new KursDBException(ex);
        }
    }

    @Override
    public void initDatabase() throws KursDBException {
        try {
            executeDBScripts("FILES/Skript_Lab11.sql", con.createStatement());
        } catch (SQLException | IOException ex) {
            Logger.getLogger(PostgresDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean executeDBScripts(String aSQLScriptFilePath, Statement stmt) throws IOException, SQLException {
        boolean isScriptExecuted = false;
        try (BufferedReader in = new BufferedReader(new FileReader(aSQLScriptFilePath))) {
            String str;
            StringBuffer sb = new StringBuffer();
            while ((str = in.readLine()) != null) {
                sb.append(str + "\n ");
            }
            stmt.execute(sb.toString());
            isScriptExecuted = true;
        } catch (Exception e) {
            System.err.println("Failed to Execute" + aSQLScriptFilePath + ". The error is" + e.getMessage());
        }
        return isScriptExecuted;
    }

    @Override
    public boolean isConnected() {
        try {
            return con.isValid(1);
        } catch (SQLException ex) {
            return false;
        }
    }

    @Override
    public List<Kunde> getKunden() throws KursDBException {
        try {
            if (getKunden == null) {
                getKunden = con.prepareStatement("Select kunde_id,kunde_zuname,kunde_vorname from Kunde");
            }
            ResultSet rs = getKunden.executeQuery();
            List<Kunde> list = new ArrayList<>();
            while (rs.next()) {
                list.add(new Kunde(rs.getInt("kunde_id"), rs.getString("kunde_zuname"), rs.getString("kunde_vorname")));
            }
            return list;
        } catch (SQLException ex) {
            throw new KursDBException(ex);
        }
    }

    @Override
    public boolean insertKunde(Kunde k) throws KursDBException {
        if (k.getId() != null) {
            throw new IllegalArgumentException();
        }
        try {
            if (insertKunde == null) {
                insertKunde = con.prepareStatement("INSERT INTO kunde(kunde_zuname,kunde_vorname) VALUES (?,?) returning kunde_id");
            }
            insertKunde.setString(1, k.getZuname());
            insertKunde.setString(2, k.getVorname());
            ResultSet rs = insertKunde.executeQuery();
            if (!rs.next()) {
                throw new KursDBException("No ID returned");
            }
            k.setId(rs.getInt("kunde_id"));
            return true;
        } catch (SQLException ex) {
            throw new KursDBException(ex);
        }
    }

    @Override
    public boolean deleteKunde(Kunde k) throws KursDBException {
        try {
            if (deleteKundePrep == null) {
                deleteKundePrep = con.prepareStatement("DELETE FROM kurs_kunde where kunde_id = ?");
            }
            if (deleteKunde == null) {
                deleteKunde = con.prepareStatement("DELETE FROM kunde where kunde_id =  ?");
            }
            deleteKundePrep.setInt(1, k.getId());
            deleteKunde.setInt(1, k.getId());
            deleteKundePrep.executeUpdate();
            deleteKunde.executeUpdate();
            return true;
        } catch (SQLException ex) {
            throw new KursDBException(ex);
        }
    }

    @Override
    public List<Dozent> getDozenten() throws KursDBException {
        try {
            if (getDozenten == null) {
                getDozenten = con.prepareStatement("Select doz_id,doz_zuname,doz_vorname from Dozent");
            }
            ResultSet rs = getDozenten.executeQuery();
            List<Dozent> list = new ArrayList<>();
            while (rs.next()) {
                list.add(new Dozent(rs.getInt("doz_id"), rs.getString("doz_zuname"), rs.getString("doz_vorname")));
            }
            return list;
        } catch (SQLException ex) {
            throw new KursDBException(ex);
        }
    }

    @Override
    public List<Kurstyp> getKurstypen() throws KursDBException {
        try {
            if (getKurstypen == null) {
                getKurstypen = con.prepareStatement("Select kurstyp_id,kurstyp_name from Kurstyp");
            }
            ResultSet rs = getKurstypen.executeQuery();
            List<Kurstyp> list = new ArrayList<>();
            while (rs.next()) {
                list.add(new Kurstyp(rs.getString("kurstyp_id").charAt(0), rs.getString("kurstyp_name")));
            }
            return list;
        } catch (SQLException ex) {
            throw new KursDBException(ex);
        }
    }

    @Override
    public List<Kurs> getKurse() throws KursDBException {
        try {
            if (getKurse == null) {
                getKurse = con.prepareStatement("Select kurs_id,kurs_typ,kurs_doz_id,kurs_bezeichnung,kurs_beginndatum from Kurs");
            }
            ResultSet rs = getKurse.executeQuery();
            List<Kurs> list = new ArrayList<>();
            while (rs.next()) {
                list.add(new Kurs(
                        rs.getInt("kurs_id"),
                        rs.getString("kurs_typ").charAt(0),
                        rs.getInt("kurs_doz_id"),
                        rs.getString("kurs_bezeichnung"),
                        rs.getDate("kurs_beginndatum")
                ));
            }
            return list;
        } catch (SQLException ex) {
            throw new KursDBException(ex);
        }
    }

    @Override
    public boolean insertKurstyp(Kurstyp kt) throws KursDBException {
        try {
            if (insertKurstyp == null) {
                insertKurstyp = con.prepareStatement("INSERT INTO kurstyp(kurstyp_id,kurstyp_name) VALUES (?,?)");
            }
            insertKurstyp.setString(1, kt.getId() + "");
            insertKurstyp.setString(2, kt.getName());
            insertKurstyp.executeUpdate();
            return true;
        } catch (SQLException ex) {
            throw new KursDBException(ex);
        }
    }

    @Override
    public boolean deleteKurstyp(Kurstyp kt) throws KursDBException {
        try {
            if (deleteKurstyp == null) {
                deleteKurstyp = con.prepareStatement("DELETE FROM kurstyp where kurstyp_id =  ?");
            }
            deleteKurstyp.setString(1, "" + kt.getId());
            deleteKurstyp.executeUpdate();
            return true;
        } catch (SQLException ex) {
            throw new KursDBException(ex);
        }
    }

    @Override
    public boolean insertKurs(Kurs kurs) throws KursDBException {
        if (kurs.getId() != null) {
            throw new IllegalArgumentException();
        }
        try {
            if (insertKurs == null) {
                insertKurs = con.prepareStatement("INSERT INTO kurs(kurs_typ,kurs_doz_id,kurs_bezeichnung,kurs_beginndatum) VALUES (?,?,?,?) returning kurs_id");
            }
            insertKurs.setString(1, kurs.getTyp() + "");
            insertKurs.setInt(2, kurs.getDoz_id());
            insertKurs.setString(3, kurs.getBezeichnung());
            insertKurs.setDate(4, kurs.getBeginn());
            ResultSet rs = insertKurs.executeQuery();
            if (!rs.next()) {
                throw new KursDBException("No ID returned");
            }
            kurs.setId(rs.getInt("kurs_id"));
            return true;
        } catch (SQLException ex) {
            throw new KursDBException(ex);
        }
    }

    @Override
    public List<Kunde> getKundenFromKurs(Kurs kurs) throws KursDBException {
        try {
            if (getKundenFromKurs == null) {
                getKundenFromKurs = con.prepareStatement("Select kunde_id,kunde_zuname,kunde_vorname from Kunde"
                        + " where kunde_id in (Select kunde_id from kurs_kunde where kurs_id = ? )");
            }
            getKundenFromKurs.setInt(1, kurs.getId());
            ResultSet rs = getKundenFromKurs.executeQuery();
            List<Kunde> list = new ArrayList<>();
            while (rs.next()) {
                list.add(new Kunde(rs.getInt("kunde_id"), rs.getString("kunde_zuname"), rs.getString("kunde_vorname")));
            }
            return list;
        } catch (SQLException ex) {
            throw new KursDBException(ex);
        }
    }

    @Override
    public boolean bucheKurs(Kunde kunde, Kurs kurs) throws KursDBException {
        try {
            if (bucheKurs == null) {
                bucheKurs = con.prepareStatement("INSERT INTO kurs_kunde(kunde_id,kurs_id) VALUES (?,?)");
            }
            bucheKurs.setInt(1, kunde.getId());
            bucheKurs.setInt(2, kurs.getId());
            bucheKurs.executeUpdate();
            return true;
        } catch (SQLException ex) {
            throw new KursDBException(ex);
        }
    }

    @Override
    public boolean storniereKurs(Kunde kunde, Kurs kurs) throws KursDBException {
        try {
            if (storniereKurs == null) {
                storniereKurs = con.prepareStatement("DELETE FROM kurs_kunde where kunde_id =  ? and kurs_id= ?");
            }
            storniereKurs.setInt(1, kunde.getId());
            storniereKurs.setInt(2, kurs.getId());
            storniereKurs.executeUpdate();
            return true;
        } catch (SQLException ex) {
            throw new KursDBException(ex);
        }
    }

}
