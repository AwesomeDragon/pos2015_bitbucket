package labor11;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class Kurs {
    Integer id;
    Character typ;
    Integer doz_id;
    String bezeichnung;
    java.sql.Date beginn;
    
    void setId(int id){
        if(this.id!=null)throw new IllegalStateException();
        this.id=id;
    }
}
