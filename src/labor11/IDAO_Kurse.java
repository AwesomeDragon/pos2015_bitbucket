package labor11;


import java.util.List;


/**
 *
 * @author reio
 */
public interface IDAO_Kurse {

  /**
   * Verbindet das DAO-Objekt mit der DB
   */
  void connect() throws KursDBException;

  /**
   * Löst die Verbindung mit der Datenbank
   * @throws KursDBException
   */
  void disconnect() throws KursDBException;
  
  void initDatabase() throws KursDBException;

  /**
   * Prüft, ob eine Verbindung besteht
   * @return true bei bestehender Verbindung, false sonst
   */
  boolean isConnected();

  /**
   * Liefert alle Kunden aus der DB als typsichere Liste
   * aufsteigend nach ihren IDs sortiert
   * @return Liste aller Kunden
   * @throws KursDBException bei einem DB Fehler
   */
  List<Kunde> getKunden() throws KursDBException;


  /**
   * Speichert einen neuen Kunden in die DB.
   * Wenn der übergebene Kunde bereits eine ID ungleich
   * 0 besitzt, wird eine IllegalArgumentException geworfen
   * @param k ein neuer Kunde
   * @return true bei Erfolg, false bei Misserfolg
   * @throws KursDBException bei einem DB Fehler
   * @throws IllegalArgumentException wenn der Kunde k eine ID ungleich 0
   *                                  besitzt
   */
  boolean insertKunde(Kunde k) throws KursDBException;


  /**
   * Loescht einen Kunden aus der DB
   * @param k zu loeschender Kunde
   * @return true bei Erfolg, false bei Misserfolg
   * @throws KursDBException bei einem DB Fehler
   */
  boolean deleteKunde(Kunde k) throws KursDBException;

  /**
   * Liefert alle Dozenten aus der DB als typsichere Liste
   * aufsteigend nach ihren IDs sortiert
   * @return Liste aller Dozenten
   * @throws KursDBException bei einem DB Fehler
   */
  List<Dozent> getDozenten() throws KursDBException;

  /**
   * Liefert alle Kurstypen aus der DB als typsichere Liste
   * aufsteigend nach ihren IDs sortiert
   * @return Liste aller Kurstypen
   * @throws KursDBException bei einem DB Fehler
   */
  List<Kurstyp> getKurstypen() throws KursDBException;

  /**
   * Liefert alle Kurse aus der DB als typsichere Liste
   * aufsteigend nach ihren IDs sortiert
   * @return Liste aller Kurse
   * @throws KursDBException bei einem DB Fehler
   */
  List<Kurs> getKurse() throws KursDBException;

  /**
   * Speichert einen neuen Kurstypen in die DB
   * @param kt ein neuer Kurstyp
   * @return true bei Erfolg, false bei Misserfolg
   * @throws KursDBException bei einem DB Fehler
   */
  boolean insertKurstyp(Kurstyp kt) throws KursDBException;

  /**
   * Loescht einen Kurstyp aus der DB
   * @param kt zu loeschender Kurstyp
   * @return true bei Erfolg, false bei Misserfolg
   * @throws KursDBException bei einem DB Fehler
   */
  boolean deleteKurstyp(Kurstyp kt) throws KursDBException;

  /**
   * Speichert einen neuen Kurs in die DB
   * @param kurs ein neuer Kurs
   * @return true bei Erfolg, false bei Misserfolg
   * @throws KursDBException bei einem DB Fehler
   */
  boolean insertKurs(Kurs kurs) throws KursDBException;

  /**
   * Liefert alle Kunden, die einen Kurs gebucht haben
   * @param kurs Kurs an den Kunden teilnehmen
   * @return Liste aller Kunden eines Kurses
   * @throws KursDBException bei einem DB Fehler
   * @throws IllegalArgumentException wenn der Kurs kurs eine ID ungleich 0
   *                                  besitzt
   */
  List<Kunde> getKundenFromKurs(Kurs kurs) throws KursDBException;

  /**
   * Bucht einen Kunden auf einen Kurs
   * @param kunde Kunde, der gebucht werden soll
   * @param kurs, der gebucht werden soll
   * @return true bei Erfolg, false sonst
   * @throws KursDBException bei einem DB Fehler
   */
  boolean bucheKurs(Kunde kunde, Kurs kurs) throws KursDBException;

  /**
   * Storniert einen Kunden fuer einen Kurs
   * @param kunde Kunde, der storniert werden soll
   * @param kurs Kurs aus dem storniert werden soll
   * @return true bei Erfolg, false sonst
   * @throws KursDBException bei einem DB Fehler
   */
  boolean storniereKurs(Kunde kunde, Kurs kurs) throws KursDBException;

}
