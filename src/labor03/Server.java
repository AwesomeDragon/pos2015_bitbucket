package labor03;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {
    private static final int PORT = 20012;
    private static final Map<String, String> map = new HashMap<>();

    public static void main(String[] args) {
        //INIT
        try (BufferedReader bf = new BufferedReader(new InputStreamReader(
                Server.class.getResourceAsStream("politische_bezirke.csv")))) {
            String tmp;
            while ((tmp = bf.readLine()) != null) {
                map.put(tmp.split(";")[1], tmp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //SERVER
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("STARTED SERVER\n---------------------------------");
            while (true) {
                try (Socket s = serverSocket.accept();
                        BufferedReader bf = new BufferedReader(new InputStreamReader(s.getInputStream()));
                        PrintStream ps = new PrintStream(s.getOutputStream())) {
                    System.out.println("Connection established with: "+s);
                    String tmp,sol;
                    while((tmp=bf.readLine())!=null){
                        if("quit".equals(tmp)){
                            ps.println("QUIT SENT");
                            System.out.println("QUIT SENT");
                            break;
                        }
                        sol = map.get(tmp);
                        if(sol==null){
                            ps.println("Not found");
                            System.out.println(tmp +" not found");
                        }else{
                            ps.println(sol);
                            System.out.println(tmp+" -> "+sol);
                        }
                    }
                    System.out.println("Closing connection with: "+s+"\n-----------------------------------------");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static int getPORT() {
        return PORT;
    }
    
}
