package labor03;

import java.io.*;
import java.util.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {
    private static final int PORT = Server.getPORT();
    private static final String HOST = "localhost";
    
    private static final String[] querys = {"Eisenstadt (Stadt)","FAIL","Krems an der Donau (Stadt)","Südoststeiermark","Hugo an der Mur"};

    public static void main(String[] args) {
        System.out.println("CLIENT\n----------------------------------------");
        try(Socket s=new Socket(HOST,PORT);
                BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
                PrintStream ps = new PrintStream(s.getOutputStream());
                BufferedReader console = new BufferedReader(new InputStreamReader(System.in,"cp1252"))
                ){
            System.out.println("Connection established with: "+s);
            String tmp;
            System.out.print("> ");
            tmp=console.readLine();
            do{
                ps.println(tmp);
                System.out.println("\t"+br.readLine());
                if("quit".equals(tmp)){
                    return;
                }
                System.out.print("> ");
            }while((tmp=console.readLine())!=null);               
        }catch(SocketException e){
            System.out.println("CONNECTION LOST");
        }catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
