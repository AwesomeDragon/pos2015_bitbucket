package labor02_1;

import java.util.concurrent.Callable;

public class Worker implements Callable<Double> {
    private int intervall_start;
    private int intervall_end;

    public Worker(int intervall_start, int intervall_end) {
        this.intervall_start = intervall_start;
        this.intervall_end = intervall_end;
    }
    
    @Override
    public Double call() {
        double solution=0;
        for(int n = intervall_start; n  <= intervall_end; n++) {
            int vz = (n % 2 == 0) ? 1 : -1;
            solution += vz / (2.0 * n + 1.0);
        }
        return solution;
    }
}
