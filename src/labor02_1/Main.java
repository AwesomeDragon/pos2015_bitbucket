package labor02_1;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static int N = 100000;
    private static int T = 10;//Number of Threads

    public static void main(String[] args){
        double solution = 0;
        ExecutorService executor = Executors.newFixedThreadPool(Math.max(2, Runtime.getRuntime().availableProcessors()));
        List<Future<Double>> sol = new ArrayList<>();

        int size = N / T;
        int remainder = N % T;
        int start = 0;
        for (int i = 0; i < T; i++) {
            int end = start + size - 1;
            if (remainder > 0) {
                end++;
                remainder--;
            }
            System.out.println("#" + i + ": " + start + " - " + end);
            sol.add(executor.submit(new Worker(start, end)));
            start = end + 1;
        }
        for (Future<Double> f : sol) {
            try {
                solution += f.get();
            } catch (InterruptedException|ExecutionException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("SOLUTION: " + solution * 4);
    }
}
