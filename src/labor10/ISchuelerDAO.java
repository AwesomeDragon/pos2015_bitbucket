/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labor10;

import java.sql.*;
import java.util.*;

/**
 *
 * @author 20120368
 */
public interface ISchuelerDAO extends AutoCloseable {

    public void connect(String url, String user, String pwd) throws SQLException;

    public void initDB(Collection<Schueler> col) throws SQLException;
    
    /**
     * Liste aller SchulerInnen einer bestimmten Klasse nach Katalognummern
     * geordnet. ¨
     *
     * @param klasse
     * @return
     * @throws SQLException
     */
    public List<Schueler> schuelerInKlasse(String klasse) throws SQLException;

    /**
     * Liste aller mannlichen / weiblichen SchuelrInnen nach
     * Klassen/Katalognummern geordnet.
     *
     * @param geschlecht
     * @return
     * @throws SQLException
     */
    public List<Schueler> schuelerNachGeschlecht(char geschlecht) throws SQLException;

    /**
     * Liefert eine Map, deren Schlussel die verschiedenen Klassenbezeichnungen
     * und deren Werte die zugehorigen Schuelerzahlen sind.
     *
     * @return Auflistung aller existierenden Klassen mit den entsprechenden
     * Schulerzahlen
     * @throws SQLException
     */
    public Map<String, Integer> getKlassen() throws SQLException;

    @Override
    public void close() throws SQLException;
}
