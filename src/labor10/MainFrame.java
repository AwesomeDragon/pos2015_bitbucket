/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labor10;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.JRadioButton;

/**
 *
 * @author 20120368
 */
public class MainFrame extends javax.swing.JFrame {
    private static String PropertiesPath = "FILES/lab10.properties";
    private static String CSVPath = "FILES/schueler.csv";
    private static ISchuelerDAO dao;
    
    public static void main(String[] args) {        
        try {
            List<Schueler> csv = Files.lines(Paths.get(CSVPath)).map(SchuelerUtils::fromCSV).collect(Collectors.toList());
            Properties prop = new Properties();
            try(FileInputStream fis = new FileInputStream(PropertiesPath)){
                prop.loadFromXML(fis);
            } 
            dao=new SchuelerDAO();
            dao.connect(prop.getProperty("url"),prop.getProperty("user"),prop.getProperty("pw"));
            dao.initDB(csv);
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }
    MainFrame(){
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTextField_Klasse = new javax.swing.JTextField();
        jRadioButton_M = new javax.swing.JRadioButton();
        jRadioButton_W = new javax.swing.JRadioButton();
        jButton_gender = new javax.swing.JButton();
        jButton_schuelerNachKlasse = new javax.swing.JButton();
        jButton_Klassen = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        display = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextField_Klasse.setText("4BHIF");
        jTextField_Klasse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_KlasseActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton_M);
        jRadioButton_M.setSelected(true);
        jRadioButton_M.setText("Male");

        buttonGroup1.add(jRadioButton_W);
        jRadioButton_W.setText("Female");

        jButton_gender.setText("SHOW BY GENDER");
        jButton_gender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_genderActionPerformed(evt);
            }
        });

        jButton_schuelerNachKlasse.setText("SHOW BY CLASS");
        jButton_schuelerNachKlasse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_schuelerNachKlasseActionPerformed(evt);
            }
        });

        jButton_Klassen.setText("SHOW CLASSES");
        jButton_Klassen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_KlassenActionPerformed(evt);
            }
        });

        display.setColumns(20);
        display.setRows(5);
        display.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        display.setDragEnabled(true);
        display.setEnabled(false);
        jScrollPane1.setViewportView(display);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton_Klassen)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton_schuelerNachKlasse)
                    .addComponent(jTextField_Klasse, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 68, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton_gender)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jRadioButton_M)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jRadioButton_W)))
                .addGap(25, 25, 25))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField_Klasse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jRadioButton_M)
                            .addComponent(jRadioButton_W))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton_gender)
                            .addComponent(jButton_schuelerNachKlasse)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jButton_Klassen)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField_KlasseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_KlasseActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_KlasseActionPerformed

    private void jButton_KlassenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_KlassenActionPerformed
        try{
            Map<String,Integer> ls = dao.getKlassen();
            String tmp="";
            for(Map.Entry<String,Integer> e : ls.entrySet()){
                tmp+=e.getKey()+" \t "+e.getValue()+"\n";
            }
            if("".equals(tmp))tmp="Kein Ergebnis";
            display.setText(tmp);
        }catch(SQLException ex){
            display.setText("SQL Fehler");
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("A PROBLEM OCCURED");
        }
    }//GEN-LAST:event_jButton_KlassenActionPerformed

    private void jButton_schuelerNachKlasseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_schuelerNachKlasseActionPerformed
       try{
            List<Schueler> ls = dao.schuelerInKlasse(jTextField_Klasse.getText());
            String tmp="";
            for(Schueler s : ls){
                tmp+=s+"\n";
            }
            if("".equals(tmp))tmp="Kein Ergebnis";
            display.setText(tmp);
        }catch(SQLException ex){
            display.setText("SQL Fehler");
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("A PROBLEM OCCURED");
        }
    }//GEN-LAST:event_jButton_schuelerNachKlasseActionPerformed

    private void jButton_genderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_genderActionPerformed
        try{
            List<Schueler> ls = dao.schuelerNachGeschlecht(jRadioButton_M.isSelected()?'M':'W');
            String tmp="";
            for(Schueler s : ls){
                tmp+=s+"\n";
            }
            if("".equals(tmp))tmp="Kein Ergebnis";
            display.setText(tmp);
        }catch(SQLException ex){
            display.setText("SQL Fehler");
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("A PROBLEM OCCURED");
        }
    }//GEN-LAST:event_jButton_genderActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JTextArea display;
    private javax.swing.JButton jButton_Klassen;
    private javax.swing.JButton jButton_gender;
    private javax.swing.JButton jButton_schuelerNachKlasse;
    private javax.swing.JRadioButton jRadioButton_M;
    private javax.swing.JRadioButton jRadioButton_W;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField_Klasse;
    // End of variables declaration//GEN-END:variables
}
