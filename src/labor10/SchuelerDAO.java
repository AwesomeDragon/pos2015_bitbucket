/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labor10;

import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 20120368
 */
public class SchuelerDAO implements ISchuelerDAO {

    Connection con;
    PreparedStatement schuelerInKlasse;
    PreparedStatement schuelerNachGeschlecht;
    PreparedStatement getKlassen;
    PreparedStatement insertSchueler;

    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SchuelerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public synchronized void connect(String url, String user, String pwd) throws SQLException {
        con = DriverManager.getConnection(url, user, pwd);
        getKlassen = con.prepareStatement("Select count(Klasse),Klasse from Schueler GROUP BY Klasse");
        schuelerInKlasse = con.prepareStatement("Select Nachname,Vorname,Geschlecht,Katalognummer,Klasse from Schueler where Klasse like ? ORDER BY Katalognummer");
        schuelerNachGeschlecht = con.prepareStatement("Select Nachname,Vorname,Geschlecht,Katalognummer,Klasse from Schueler where Geschlecht like ? ORDER BY Klasse,Katalognummer");
        insertSchueler = con.prepareStatement("INSERT INTO Schueler(Nachname,Vorname,Geschlecht,Katalognummer,Klasse) VALUES(?,?,?,?,?)");

    }

    public void initDB(Collection<Schueler> col) throws SQLException {
        Statement st = con.createStatement();
        st.execute("drop table if exists Schueler");
        st = con.createStatement();
        st.execute("create table Schueler(Nachname varchar(50),Vorname varchar(50),Geschlecht varchar(1), Katalognummer int, Klasse varchar(10))");
        for (Schueler s : col) {
            insertSchueler.setString(1, s.getNachname());
            insertSchueler.setString(2, s.getVorname());
            insertSchueler.setString(3, "" + s.getGeschlecht());
            insertSchueler.setInt(4, s.getNr());
            insertSchueler.setString(5, s.getKlasse());
            insertSchueler.execute();
        }
    }

    @Override
    public synchronized List<Schueler> schuelerInKlasse(String klasse) throws SQLException {
        checkConnection();
        schuelerInKlasse.setString(1, klasse);
        System.out.println(schuelerInKlasse);
        schuelerInKlasse.execute();
        return resultSetToList(schuelerInKlasse.getResultSet());
    }

    @Override
    public synchronized List<Schueler> schuelerNachGeschlecht(char geschlecht) throws SQLException {
        checkConnection();
        schuelerNachGeschlecht.setString(1, "" + geschlecht);
        System.out.println(schuelerNachGeschlecht);
        schuelerNachGeschlecht.execute();
        return resultSetToList(schuelerNachGeschlecht.getResultSet());
    }

    private List<Schueler> resultSetToList(ResultSet rs) throws SQLException {
        List<Schueler> ls = new ArrayList<>();
        while (rs.next()) {
            //RS Order:     (Nachname,Vorname,Geschlecht,Katalognummer,Klasse)
            //constructor: String klasse, int nr, String nachname, String vorname, char geschlecht
            ls.add(new Schueler(
                    rs.getString(5), rs.getInt(4), rs.getString(1), rs.getString(2), rs.getString(3).charAt(0)
            ));
        }
        return ls;
    }

    @Override
    public synchronized Map<String, Integer> getKlassen() throws SQLException {
        checkConnection();
        Map<String, Integer> map = new TreeMap<>();
        ResultSet rs = getKlassen.executeQuery();
        while (rs.next()) {
            map.put(rs.getString(2), rs.getInt(1));
        }
        return map;
    }

    @Override
    public synchronized void close() throws SQLException {
        con.close();
    }

    private void checkConnection() {
        if (con == null) {
            throw new IllegalStateException("Not connected");
        }
    }
}
